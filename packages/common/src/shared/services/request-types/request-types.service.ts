import { Injectable } from '@angular/core';
import { Args } from '@themost/client';

/**
 *
 * Request types service
 *
 * Registrar stuff accepts requests. Requests can be organized by category.
 * This service stores the list of the available requests.
 *
 * This service can be used to read the available categories, the entries of those
 * categories and update the list of the categories.
 *
 * An example of a request category type is the document category (e.g. transcript).
 *
 */


/**
 *
 * RequestTypeItem interface
 *
 * Represents a request type that is going to be used by request type service
 *
 * @param {string} name The name of an item, commonly a translation key e.g. 'Requests.Transcript'
 * @param {string} alternateName the key of the item e.g. transcript
 * @param {string} description An optional description
 * @param {string} category The category of this item e.g. DocumentRequests, OtherRequests, Questions etc
 * @param {string} entryPoint The entry point that is going to be used for this request
 *
 */
export declare interface RequestTypeItem {
  name: string;
  alternateName: string;
  category: string;
  entryPoint: string;
  description?: string;
}

/**
 *
 * RequestTypeServiceBase
 *
 */
declare interface RequestTypeServiceBase {
  add(item: RequestTypeItem): void;
  addRange(...items: Array<RequestTypeItem>): void;
  remove(item: RequestTypeItem): RequestTypeItem;
  getItems(): Array<RequestTypeItem>;
}


/**
 *
 * RequestTypesServiceService
 *
 * Read the available categories, the entries of those categories and update the list of the categories.
 *
 */
@Injectable({
  providedIn: 'root'
})
export class RequestTypesService implements RequestTypeServiceBase {

  /**
   * @param {Array<any>} requestTypes The list of the request categories
   */
  private requestTypes: Array<RequestTypeItem> = [];

  constructor() { }

  /**
   *
   * Adds a new item to the service list
   *
   * @param {RequestTypeItem} item The request type item to be added
   *
   */
  add(item: RequestTypeItem): void {
    Args.notNull(item, 'RequestTypeItem');
    Args.notNull(item.name, 'RequestTypeItem name');
    Args.notNull(item.alternateName, 'RequestTypeItem alternateName');
    Args.notNull(item.category, 'RequestTypeItem category');
    Args.notNull(item.entryPoint, 'RequestTypeItem entryPoint');

    const re = new RegExp(`^${item.alternateName}$`, 'ig');
    const itemExist = this.requestTypes.some((existingItem) =>
      re.test(existingItem.alternateName)
    );

    if (!itemExist) {
      this.requestTypes.push(item);
    }
  }

  /**
   *
   * Adds a number of items to the service list
   *
   * @param {Array<RequestTypeItem>} item The array of the items to be included.
   *
   */
  addRange(...items: Array<RequestTypeItem>): void {
    for (const item of items) {
      this.add(item);
    }
  }

  /**
   *
   * Remove
   *
   * Removes an item from the list and returns the deleted item
   * Returns null when the item is not found
   *
   * @param {RequestTypeItem} item The item to be deleted
   *
   */
  remove(item: RequestTypeItem): RequestTypeItem {
    Args.notNull(item, 'RequestTypeItem');

    const re = new RegExp(`^${item.alternateName}$`, 'ig');
    const findIndex = this.requestTypes.findIndex((existingItem) =>
      re.test(existingItem.alternateName)
    );

    if (findIndex < 0) {
      return null;
    }

    // remove item at index found and return the deleted item
    return this.requestTypes.splice(findIndex, 1)[0];
  }

  /**
   *
   * Remove
   *
   * Removes an item from the list and returns the deleted item
   * Returns null when the item is not found
   *
   * @param {string} alternateName The alternateName of item to be deleted
   *
   */
  removeByName(alternateName: string): RequestTypeItem {
    const re = new RegExp(`^${alternateName}$`, 'ig');
    const targetItem = this.requestTypes.find((existingItem) =>
      re.test(existingItem.alternateName)
    );

    if (!targetItem) {
      return null;
    }

    return this.remove(targetItem);
  }

  /**
   *
   * getItems
   *
   * Returns the current state of the request items list
   *
   */
  getItems(): Array<RequestTypeItem> {
    return [...this.requestTypes];
  }
}
