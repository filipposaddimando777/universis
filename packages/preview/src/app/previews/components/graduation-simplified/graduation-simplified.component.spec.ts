import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationSimplifiedComponent } from './graduation-simplified.component';

describe('GraduationSimplifiedComponent', () => {
  let component: GraduationSimplifiedComponent;
  let fixture: ComponentFixture<GraduationSimplifiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationSimplifiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationSimplifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
