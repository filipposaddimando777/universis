import { Component, OnInit, Input } from '@angular/core';
import { template } from 'lodash';
import { config } from 'process';
import { ActivatedRoute } from '@angular/router';

export interface ActionButtonItem {
  title: string;
  href?: string;
  click?: any;
  disabled?: boolean;
}

@Component({
  selector: 'app-actions-button',
  templateUrl: './actions-button.component.html',
  styleUrls: ['./actions-button.component.scss']
})
export class ActionsButtonComponent implements OnInit {

  @Input() actions: any;
  @Input() edit: any;

  constructor() { }

  ngOnInit() {

  }

}
