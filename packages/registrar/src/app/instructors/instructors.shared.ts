import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {InstructorsDashboardOverviewCurrentClassesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-currentClasses/instructors-dashboard-overview-currentClasses.component';
import {InstructorsDashboardOverviewExamsComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-exams/instructors-dashboard-overview-exams.component';
import {InstructorsDashboardOverviewThesesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-theses/instructors-dashboard-overview-theses.component';
import {InstructorsDashboardOverviewSendingGradesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-sendingGrades/instructors-dashboard-overview-sendingGrades.component';
import {TooltipModule} from 'ngx-bootstrap';
import {InstructorsDashboardOverviewProfileComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-profile/instructors-dashboard-overview-profile.component';
import {InstructorsDashboardOverviewSupervisedComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-supervised/instructors-dashboard-overview-supervised.component';
import * as INSTRUCTORS_LIST from './components/instructors-table/instructors-table.config.json';
import {
  InstructorDefaultTableConfigurationResolver, InstructorTableConfigurationResolver,
  InstructorTableSearchResolver
} from './components/instructors-table/instructor-table-config.resolver';
import {
  InstructorDashboardExamsConfigurationResolver, InstructorDashboardExamsSearchResolver,
  InstructorDefaultDashboardExamsConfigurationResolver
} from './components/instructors-dashboard/instructors-dashboard-exams/instructor-dashboard-exams-config.resolver';
import { InstructorDashboardClassesConfigurationResolver, InstructorDashboardClassesSearchResolver, InstructorDefaultDashboardClassesConfigurationResolver } from './components/instructors-dashboard/instructors-dashboard-classes/instructor-dashboard-classes-config.resolver';
import {MessagesModule} from '../messages/messages.module';
import {RouterModule} from '@angular/router';
import {
  InstructorDashboardThesesConfigurationResolver,
  InstructorDashboardThesesRequestsConfigurationResolver
} from './components/instructors-dashboard/instructors-dashboard-theses/instructors-dashboard-theses-config.resolver';
import {
  InstructorDashboardStudentsConfigurationResolver,
  InstructorDashboardStudentsSearchResolver, InstructorDefaultDashboardStudentsConfigurationResolver
} from './components/instructors-dashboard-students/instructors-dashboard-students-config.resolver';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        TooltipModule.forRoot(),
        MessagesModule,
        RouterModule

    ],
  declarations: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent
  ],
  exports: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent
  ],
  providers: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent,
    InstructorDefaultTableConfigurationResolver,
    InstructorTableConfigurationResolver,
    InstructorTableSearchResolver,
    InstructorDashboardExamsConfigurationResolver,
    InstructorDashboardExamsSearchResolver,
    InstructorDefaultDashboardExamsConfigurationResolver,
    InstructorDashboardClassesConfigurationResolver,
    InstructorDashboardClassesSearchResolver,
    InstructorDefaultDashboardClassesConfigurationResolver,
    InstructorDashboardThesesConfigurationResolver,
    InstructorDashboardThesesRequestsConfigurationResolver,
    InstructorDashboardStudentsConfigurationResolver,
    InstructorDashboardStudentsSearchResolver,
    InstructorDefaultDashboardStudentsConfigurationResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class InstructorsSharedModule implements OnInit {

  public static readonly InstructorsList = INSTRUCTORS_LIST;
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading instructors shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/instructors.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
