
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';

export class InstructorDashboardStudentsConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-exams.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`packages/registrar/src/app/instructors/components/instructors-dashboard-students/instructors-dashboard-students.config.list.json`);
        });
    }
}

export class InstructorDashboardStudentsSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-students.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`packages/registrar/src/app/instructors/components/instructors-dashboard-students/instructors-dashboard-students.search.list.json`);
            });
    }
}
export class InstructorDefaultDashboardStudentsConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`packages/registrar/src/app/instructors/components/instructors-dashboard-students/instructors-dashboard-students.config.list.json`);
    }
}
