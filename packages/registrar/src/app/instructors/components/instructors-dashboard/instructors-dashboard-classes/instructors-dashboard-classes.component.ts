import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as INSTRUCTORS_CLASSES_LIST_CONFIG from './instructors-dashboard-classes.config.list.json';
import { ActivatedTableService } from '../../../../tables/tables.activated-table.service';
import { Subscription } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';

@Component({
  selector: 'app-instructors-dashboard-classes',
  templateUrl: './instructors-dashboard-classes.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardClassesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>INSTRUCTORS_CLASSES_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  instructorID: any = this._activatedRoute.snapshot.params.id;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _translateService: TranslateService,
    private _toastService: ToastService
  ) {
    //
  }

  async ngOnInit() {

    this._activatedTable.activeTable = this.classes;

    this.classes.query = this._context.model('courseClassInstructors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseClass($expand=course,year,period)')
      .orderByDescending('courseClass/year')
      .thenByDescending('courseClass/period')
      .prepare();

    this.classes.config = AdvancedTableConfiguration.cast(INSTRUCTORS_CLASSES_LIST_CONFIG);

    // do reload by using hidden fragment e.g. /classes#reload
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.classes.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      if (data.tableConfiguration) {
        this.classes.config = data.tableConfiguration;
        this.classes.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.classes && this.classes.selected && this.classes.selected.length) {
      // get items to remove
      const items = this.classes.selected.map(item => {
        return {
          instructor: this.instructorID,
          courseClass: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Instructors.RemoveClassesTitle'),
        this._translateService.instant('Instructors.RemoveClassesMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseClassInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Instructors.RemoveClassMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Instructors.RemoveClassMessage.one' : 'Instructors.RemoveClassMessage.many')
                  , { value: items.length })
              );
              this.classes.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

    }
  }
}
