import {Injectable} from '@angular/core';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class InstructorTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./instructors-table.config.list.json`);
        });
    }
}

export class InstructorTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./instructors-table.search.list.json`);
            });
    }
}

export class InstructorDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./instructors-table.config.list.json`);
    }
}
