import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorsHomeComponent } from './components/instructors-home/instructors-home.component';
import {InstructorsRoutingModule} from './instructors.routing';
import {InstructorsSharedModule} from './instructors.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { InstructorsTableComponent } from './components/instructors-table/instructors-table.component';
import { InstructorsDashboardComponent } from './components/instructors-dashboard/instructors-dashboard.component';
import { InstructorsRootComponent } from './components/instructors-root/instructors-root.component';
import {InstructorsDashboardOverviewComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview.component';
import {InstructorsDashboardGeneralComponent} from './components/instructors-dashboard/instructors-dashboard-general/instructors-dashboard-general.component';
import {InstructorsDashboardClassesComponent} from './components/instructors-dashboard/instructors-dashboard-classes/instructors-dashboard-classes.component';
import {InstructorsDashboardExamsComponent} from './components/instructors-dashboard/instructors-dashboard-exams/instructors-dashboard-exams.component';
import {InstructorsDashboardThesesComponent} from './components/instructors-dashboard/instructors-dashboard-theses/instructors-dashboard-theses.component';
import {InstructorsDashboardTeachingComponent} from './components/instructors-dashboard/instructors-dashboard-teaching/instructors-dashboard-teaching.component';
import {TooltipModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import {ClassesSharedModule} from '../classes/classes.shared';
import {ExamsSharedModule} from '../exams/exams.shared';
import {ThesesSharedModule} from '../theses/theses.shared';
import {ElementsModule} from '../elements/elements.module';
import {MostModule} from '@themost/angular';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {RouterModalModule} from '@universis/common/routing';
import {InstructorAddClassComponent} from './components/instructors-dashboard/instructors-dashboard-classes/instructor-add-class-component';
import {InstructorAddExamsComponent} from './components/instructors-dashboard/instructors-dashboard-exams/instructor-add-exams-component';
import {InstructorsDashboardStudentsComponent} from './components/instructors-dashboard-students/instructors-dashboard-students.component';
import {CounselorsSharedModule} from '../counselors/counselors.shared';

@NgModule({
  imports: [
      CommonModule,
      TranslateModule,
      InstructorsSharedModule,
      InstructorsRoutingModule,
      ClassesSharedModule,
      ExamsSharedModule,
      TablesModule,
      ThesesSharedModule,
      ElementsModule,
      SharedModule,
      FormsModule,
      TooltipModule.forRoot(),
      MostModule,
      RegistrarSharedModule,
      RouterModalModule,
      CounselorsSharedModule
  ],
  declarations: [InstructorsHomeComponent,
    InstructorsTableComponent,
    InstructorsDashboardComponent,
    InstructorsRootComponent,
    InstructorsDashboardGeneralComponent,
    InstructorsDashboardClassesComponent,
    InstructorsDashboardTeachingComponent,
    InstructorsDashboardExamsComponent,
    InstructorsDashboardThesesComponent,
    InstructorsDashboardTeachingComponent,
    InstructorsDashboardOverviewComponent,
    InstructorAddClassComponent,
    InstructorAddExamsComponent,
    InstructorsDashboardStudentsComponent
    ],

    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InstructorsModule { }
