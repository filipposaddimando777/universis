import {NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {
  CounselorsTableSearchResolver,
  CounselorsTableConfigurationResolver, CounselorsDefaultTableConfigurationResolver
} from './components/counselors-table/counselors-table-config.resolver';
import { AdvancedFormsModule } from '@universis/forms';
import {TablesModule} from '../tables/tables.module';
import { MostModule } from '@themost/angular';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    TablesModule
  ],
  declarations: [],
  providers: [
    CounselorsTableSearchResolver,
    CounselorsTableConfigurationResolver,
    CounselorsDefaultTableConfigurationResolver
  ],
  exports: []
})
export class CounselorsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading student counselors shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach(language => {
      import(`./i18n/counselors.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
