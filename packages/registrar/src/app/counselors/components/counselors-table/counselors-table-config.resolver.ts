import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class CounselorsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./counselors-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`packages/registrar/src/app/counselors/components/counselors-table/counselors-table.config.list.json`);
        });
    }
}

export class CounselorsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`packages/registrar/src/app/counselors/components/counselors-table/counselors-table.search.list.json`);
            });
    }
}

export class CounselorsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`packages/registrar/src/app/counselors/components/counselors-table/counselors-table.config.list.json`);
    }
}
