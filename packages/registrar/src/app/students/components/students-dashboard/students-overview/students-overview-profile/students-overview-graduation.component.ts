import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-graduation',
  templateUrl: './students-overview-graduation.component.html'
})
export class StudentsOverviewGraduationComponent implements OnInit, OnDestroy  {

  public student;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('person($expand=gender), department, studyProgram, user')
        .getItem()
        .then((value) => {
          this.student = value;
          // check if student is declared and get info from studentDeclaration model
          if (this.student && this.student.studentStatus && this.student.studentStatus.alternateName === 'declared') {
            this._context.model('StudentDeclarations')
              .where('student').equal(this.studentId)
              .getItem().then((declaredInfo) => {
              if (declaredInfo) {
                this.student.declaredInfo = declaredInfo;
              }
            });
          }
        });
    }
  }


  async ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
