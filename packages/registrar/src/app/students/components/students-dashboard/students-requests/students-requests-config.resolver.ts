import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class StudentsRequestsConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./students-requests.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./students-requests.config.list.json`);
      });
  }
}

export class StudentsRequestsSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./students-requests.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./students-requests.search.list.json`);
      });
  }
}

export class StudentsDefaultRequestsConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./students-requests.config.list.json`);
  }
}
