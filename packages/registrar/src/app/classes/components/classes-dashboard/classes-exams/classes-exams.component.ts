import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_EXAMS_LIST_CONFIG from './classes-exams.config.list.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';

@Component({
  selector: 'app-classes-preview-exams',
  templateUrl: './classes-exams.component.html'
})
export class ClassesExamsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_EXAMS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('exams') exams: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.exams;

      this.exams.query = this._context.model('CourseExamClasses')
        .where('courseClass').equal(this.courseClassID)
        .prepare();

      this.exams.config = AdvancedTableConfiguration.cast(CLASSES_EXAMS_LIST_CONFIG);
      this.exams.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.exams.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.exams.config = data.tableConfiguration;
          this.exams.ngOnInit();
        }
        /*      if (data.searchConfiguration) {
                this.search.form = data.searchConfiguration;
                this.search.ngOnInit();
              }*/
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;

  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  remove() {
    if (this.exams && this.exams.selected && this.exams.selected.length) {
      const items = this.exams.selected.map( item => {
        return {
          courseClass: this.courseClassID,
          courseExam: item.examId
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Classes.RemoveExamTitle'),
        this._translateService.instant('Classes.RemoveExamMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('CourseExamClasses').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Classes.RemoveExamsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Classes.RemoveExamsMessage.one' : 'Classes.RemoveExamsMessage.many')
                , { value: items.length })
            );
            this.exams.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }

  async getExamAdditionConfirmation() {
    // get confirmation regarding the exam addition procedure
    return this._modalService.showDialog(
      this._translateService.instant('Classes.AddExam'),
      this._translateService.instant('Classes.AddExamsMessage'),
      DIALOG_BUTTONS.OkCancel).then( result => {
      // if the user confirms the action, proceed
      if (result === 'ok') {
        this.linkCourseExamsToCourseClassAndAdd();
      }
    });
  }

  async linkCourseExamsToCourseClassAndAdd() {
      // get courseClass
      const courseClass = await this._context.model('CourseClasses')
        .where('id').equal(this.courseClassID)
        .select('id', 'course', 'year/id as year', 'period/id as period')
        .getItem();
      // get exam periods and filter allowed periods with courseClass period
      let examPeriods = await this._context.model('ExamPeriods').getItems();
      examPeriods = examPeriods.filter(x => {
        return (courseClass.period & x.periods) === courseClass.period;
      }).map(y => {
        return y.id;
      });
      // get courseExams for specific year, course and filter with allowed exam periods
      let availableCourseExams = await this._context.model('CourseExams')
        .where('course').equal(courseClass.course)
        .and('year').equal(courseClass.year)
        .getItems();

      availableCourseExams = availableCourseExams.filter(x => {
        return examPeriods.indexOf(x.examPeriod) >= 0;
      }).map(y => {
        return {
          courseClass: courseClass.id,
          courseExam: y.id
        };
      });

      // get courseExamClasses
      const courseExamClasses = await this._context.model('CourseExamClasses')
        .where('courseClass/id').equal(this.courseClassID)
        .select('courseClass as course', 'courseExam as exam')
        .getItems();

      // remove already linked courseExams
      availableCourseExams = availableCourseExams.filter(available => {
        return !courseExamClasses.some(existing => {
          return available.courseClass === existing.course && available.courseExam === existing.exam;
        });
      });

      // if there are no available course exams to be linked, inform the user and return.
      if (availableCourseExams.length === 0) {
        return this._toastService.show(
          this._translateService.instant('Classes.AddExamMessage.title'),
          this._translateService.instant('Classes.AddExamMessage.noAvailableExams')
        );
      }

      // if any, save available courseClass-courseExam connections
      await this._context.model('CourseExamClasses').save(availableCourseExams);

      // inform the user about the result
      this._toastService.show(
        this._translateService.instant('Classes.AddExamMessage.title'),
        this._translateService.instant((availableCourseExams.length === 1 ?
          'Classes.AddExamMessage.one' : 'Classes.AddExamMessage.many')
          , { value: availableCourseExams.length })
      );

      // and -finally- fetch the updated table
      return this.exams.fetch();
  }
}
