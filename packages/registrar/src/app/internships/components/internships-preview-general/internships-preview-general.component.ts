import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';
import { AppEventService, ErrorService } from '@universis/common';
import { AdvancedSelectService } from '../../../tables/components/advanced-select/advanced-select.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-internships-preview-general',
  templateUrl: './internships-preview-general.component.html'
})
export class InternshipsPreviewGeneralComponent implements OnInit, OnDestroy {

  @Input() model: any;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  public previousStudentId: any;
  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
    private _selectService: AdvancedSelectService,
    private _translateService: TranslateService,
    private _errorService: ErrorService) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Internships')
        .where('id').equal(params.id)
        // tslint:disable-next-line:max-line-length
        .expand('status,company,internshipPeriod,department,student($expand=person,studyProgram,user,department,inscriptionYear,studentStatus)')
        .getItem();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
      if (fragment === 'reload') {
        if (this.model && this.model.id) {
          this._context.model('Internships')
            .where('id').equal(this.model.id)
            // tslint:disable-next-line:max-line-length
            .expand('status,company,internshipPeriod,department,student($expand=person,studyProgram,user,department,inscriptionYear,studentStatus)')
            .getItem().then((result) => {
              this.model = result;
            });
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  addStudent() {
    this._selectService.select({
      modalTitle: this._translateService.instant('Internships.SelectStudent'),
      tableConfigSrc: 'assets/tables/Internships/select-student.json'
    }).then((result) => {
      if (result.result === 'ok') {
         // get selected item
         const selectedStudent = result.items[0];
         if (selectedStudent) {
           const item = Object.assign({}, this.model, {
             student: selectedStudent,
             status: {
               alternateName: 'active'
             }
           });
           this._context.model('Internships').save(item).then(() => {
              this._router.navigate(['.'], {
                fragment: 'reload',
                relativeTo: this._activatedRoute,
                replaceUrl: false
              });
           }).catch((err) => {
             this._errorService.showError(err, {
               continueLink: '.'
             });
           });
         }
      }
    });
  }

}
