import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {combineLatest, Observable, Subscription} from 'rxjs';
import {
  AppEventService,
  ConfigurationService,
  ErrorService,
  GradeScale,
  LoadingService,
  ModalService,
  TemplatePipe
} from '@universis/common';
import {AdvancedTableConfiguration} from '../../../../../tables/components/advanced-table/advanced-table.component';
import * as GROUP_COURSES_LIST_CONFIG from '../group-courses-table.config.json';
import {AddGroupCoursesComponent} from '../add-group-courses/add-group-courses.component';
import {AdvancedFilterValueProvider} from '../../../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import {GroupCoursesDetailsPercentComponent} from './group-courses-details-percent/group-courses-details-percent.component';


@Component({
  selector: 'app-group-courses-details',
  templateUrl: './group-courses-details.component.html'
})
export class GroupCoursesDetailsComponent implements OnInit, OnDestroy {

  studyProgram: any = this._activatedRoute.snapshot.params.id;

  public studyProgramsId;
  public groupCourses;
  public programGroupId;
  public programGroup;
  public childrenGroups: Array<any>;
  public isLoading: boolean = true;
  private subscription: Subscription;
  private eventSubscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _advancedFilter: AdvancedFilterValueProvider,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _configService: ConfigurationService,
              private _errorService: ErrorService,
              private _appEventService: AppEventService) {
  }

  async ngOnInit() {
    this.subscription = combineLatest([this._activatedRoute.params, this._activatedRoute.parent.params]).subscribe(async (params) => {
      this.isLoading = true;
      this._loadingService.showLoading();
      this.studyProgramsId = params[1].id;
      this.programGroupId = params[0].id;

      this.programGroup = await this._context.model('ProgramGroups')
        .where('program').equal(this.studyProgramsId)
        .and('id').equal(this.programGroupId).getItem();

      if (this.programGroup.hasOwnProperty('groupType') && this.programGroup.groupType === 1 && this.programGroup.hasOwnProperty('parentGroup') && !this.programGroup.parentGroup) {
        this.childrenGroups = await this._context.model('ProgramGroups')
          .where('program').equal(this.studyProgramsId)
          .and('parentGroup').equal(this.programGroup.id)
          .getItems();
      } else {
        await this.fetchCourses();
      }
      this._loadingService.hideLoading();
      this.isLoading = false;
    });
    this.eventSubscription = this._appEventService.change.subscribe(async(x) => {
      if(x.model === 'StudyProgramCourses'){
        await this.fetchCourses()
      }
    });
  }

  async fetchCourses(){
    this.groupCourses  = await this._context.model('StudyProgramCourses')
      .where('studyProgram').equal(this.studyProgramsId)
      .and('programGroup').equal(this.programGroupId)
      .expand('course($expand=courseArea)')
      .getItems();
  }

  async editCourses(remove: boolean = false) {
    this._advancedFilter.values = {...this._advancedFilter.values,
      'programGroup': this.programGroup.id,
      'program': this.programGroup.program
    };
    this._modalService.openModalComponent(AddGroupCoursesComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: false,
      initialState: {
        studyProgram: this.programGroup.program,
        programGroup: this.programGroup.id,
        modalTitle: 'StudyPrograms.EditCourses',
        removeCourses: remove,
        execute: this.executeAction()
      }
    });
  }

  executeAction() {
    return new Observable((observer) => {
      this._loadingService.showLoading();
      // get add courses component
      const component = <AddGroupCoursesComponent>this._modalService.modalRef.content;
      // and submit
      this._context.model('ProgramCourses').save(Array.from(component.items)).then(async() => {
        await this.ngOnInit();
        this._loadingService.hideLoading();
        delete this._advancedFilter.values['programGroup'];
        delete this._advancedFilter.values['program'];
        observer.next();
      }).catch((err) => {
        this._loadingService.hideLoading();
        observer.error(err);
      });
    });
  }

  editFactors() {
    try {
      this._modalService.openModalComponent(GroupCoursesDetailsPercentComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          groupCourses: this.groupCourses
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  ngOnDestroy() {
    if(this.subscription && !this.eventSubscription.closed){
      this.subscription.unsubscribe();
    }
    if(this.eventSubscription && !this.eventSubscription.closed){
      this.eventSubscription.unsubscribe();
    }
  }
}
