import {Component, Directive, EventEmitter, Input, OnDestroy, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate} from '../../../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ErrorService, ToastService} from '@universis/common';
import {AdvancedFilterValueProvider} from '../../../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-group-add-course',
  template: AdvancedTableModalBaseTemplate
})
export class GroupAddCourseComponent extends AdvancedTableModalBaseComponent {
  @Input() group: any;

  constructor(_router: Router, _activatedRoute: ActivatedRoute,
              protected _context: AngularDataContext,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _datePipe: DatePipe) {
    super(_router, _activatedRoute, _context, _advancedFilterValueProvider, _datePipe);
    // set default title
    this.modalTitle = 'StudyPrograms.AddCourse';
  }
  hasInputs(): Array<string> {
    return [ 'group' ];
  }
  ok(): Promise<any> {
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      items = selected.map( course => {
        return {
          programGroup: this.group.id,
          studyProgram: course.programID,
          course: course.courseId
        };
      });
      return this._context.model('StudyProgramCourses')
        .save(items)
        .then( result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('Theses.AddMembersMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Theses.AddMembersMessage.one' : 'Theses.AddMembersMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch( err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }
}
