import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {AppEventService} from '@universis/common';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-edit-courses',
  templateUrl: './edit-courses.component.html'
})
export class EditCoursesComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  public loading = false;
  public lastError;
  @Input() execute: Observable<any>;
  @Input() items: Array<any>;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  @Input() formEditName = 'SpecializationCourses/edit';
  @Input() toastHeader = 'StudyPrograms.EditCourses';
  @Input() courseProperties;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit() {
    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe(() => {
      if (this.formComponent.form.config) {
        // find submit button
        const findButton = this.formComponent.form.form.components.find(component => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          (<any>findButton).hidden = true;
        }
      }
    });
    this.formComponent.formName = this.formEditName;
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        this.okButtonDisabled = !event.isValid;
      }
    });
    // format form data
    if (this.items && this.items.length) {
      const first = this.items[0];
      this.formComponent.data = {
        semester: first.semester,
        units: first.units,
        ects: first.ects,
        courseType: first.courseType,
        coefficient: first.coefficient
      };
      if(this.courseProperties && typeof this.courseProperties === 'object'
        && !Array.isArray(this.courseProperties)) {
        this.formComponent.data = {...this.formComponent.data, ...this.courseProperties};
      }
    }
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return  this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  ok(): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
        this.loading = false;
        this._loadingService.showLoading();
        this.lastError = null;
        // alter items
        const data = this.formComponent.form.formio.data;
        this.items.forEach((item) => {
          Object.assign(item, data);
        });
        this.execute.subscribe((result) => {
          this.loading = false;
          this._loadingService.hideLoading();
          if (this._modalService.modalRef) {
            this._modalService.modalRef.hide();
          }
          this._toastService.show(
              this._translateService.instant(this.toastHeader),
              this._translateService.instant('Settings.OperationCompleted'),
              true,
              3000
          );
          return resolve();
        }, (err) => {
          this.loading = false;
          this._loadingService.hideLoading();
          this.lastError = err;
          return resolve();
        });
      });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
    }
  }

  onChange($event: any) {

  }

}
