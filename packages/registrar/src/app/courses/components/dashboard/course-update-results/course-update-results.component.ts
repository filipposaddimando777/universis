import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as COURSE_UPDATE_RESULTS_LIST_CONFIG from './course-update-results-table.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {RequestActionComponent} from '../../../../requests/components/request-action/request-action.component';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';


@Component({
  selector: 'app-course-update-results',
  templateUrl: './course-update-results.component.html',
  styleUrls: ['./course-update-results.component.scss']
})
export class CourseUpdateResultsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>COURSE_UPDATE_RESULTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public courseId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('StudentCourseUpdateResults')
        .asQueryable()
        .where('action/course').equal(params.id)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(COURSE_UPDATE_RESULTS_LIST_CONFIG);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'StudentCourseUpdateResults';
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form =  Object.assign(data.searchConfiguration, { course: this.courseId });
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
