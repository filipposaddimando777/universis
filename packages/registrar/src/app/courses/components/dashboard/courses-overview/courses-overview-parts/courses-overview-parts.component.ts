import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { AppEventService, ErrorService, ModalService } from '@universis/common';
import {Subscription} from 'rxjs';
import { EditCoursePartsFactorsComponent } from '../edit-course-parts-factors/edit-course-parts-factors.component';

@Component({
  selector: 'app-courses-overview-parts',
  templateUrl: './courses-overview-parts.component.html',
  styleUrls: ['./courses-overview-parts.component.scss']
})
export class CoursesOverviePartsComponent implements OnInit, OnDestroy {
  @Input() currentYear: any;
  public courseId: any;
  public model: any;
  public parts: any;
  private subscription: Subscription;
  private changeSubscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _appEvent: AppEventService) {
}


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this.parts = await this.fetchParts();
    });
    this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
      if (event && event.model === 'Courses') {
        this.parts = await this.fetchParts();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  openEditFactorsModal() {
    try {
      this._modalService.openModalComponent(EditCoursePartsFactorsComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          courseParts: this.parts
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async complexCourseExistsInStudentCourses(): Promise<boolean> {
    const studentCourses = await this._context.model('StudentCourses')
    .asQueryable()
    .where('course/id').equal(this.courseId)
    .select('count(id) as coursesCount')
    .getItem();
    return studentCourses.coursesCount > 0;
  }

  async fetchParts() {
    return this._context.model('Courses')
    .where('parentCourse').equal(this.courseId)
    .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory').getItems();
  }
}
