import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class ExamsParticipationsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-participations-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./exams-participations-table.config.list.json`);
        });
    }
}

export class ExamsParticipationsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-participations-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./exams-participations-table.search.list.json`);
            });
    }
}

export class ExamsParticipationsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./exams-participations-table.config.list.json`);
    }
}
