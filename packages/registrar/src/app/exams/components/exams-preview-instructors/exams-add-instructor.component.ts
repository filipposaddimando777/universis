import { Component, Directive, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate } from '../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from './../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-exam-add-instructor',
    template: AdvancedTableModalBaseTemplate
})
export class ExamsAddInstructorComponent extends AdvancedTableModalBaseComponent {

    @Input() courseExam: any;

    constructor(_router: Router, private _activatedRoute: ActivatedRoute,
        _context: AngularDataContext,
        private _errorService: ErrorService,
        private _toastService: ToastService,
        private _translateService: TranslateService,
        protected advancedFilterValueProvider: AdvancedFilterValueProvider,
        protected datePipe: DatePipe,
        ) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
        // set default title
        this.modalTitle = 'Exams.AddInstructor';
    }

    hasInputs(): Array<string> {
        return ['courseExam'];
    }

    ok(): Promise<any> {
        // get selected items
        const selected = this.advancedTable.selected;
        let items = [];
        if (selected && selected.length > 0) {
            // try to add classes
            items = selected.map(instructor => {
                return {
                    instructor: instructor,
                    courseExam: this.courseExam
                };
            });
            return this._context.model('courseExamInstructors')
                .save(items)
                .then(result => {
                    // add toast message
                    this._toastService.show(
                        this._translateService.instant('Exams.AddInstructorsMessage.title'),
                        this._translateService.instant((items.length === 1 ?
                            'Exams.AddInstructorsMessage.one' : 'Exams.AddInstructorsMessage.many')
                            , { value: items.length })
                    );
                    return this.close({
                        fragment: 'reload',
                        skipLocationChange: true
                    });
                }).catch(err => {
                    this._errorService.showError(err);
                });
        }
        return this.close();
    }
}
