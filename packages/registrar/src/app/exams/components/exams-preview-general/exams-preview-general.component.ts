import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-general',
  templateUrl: './exams-preview-general.component.html',
  styleUrls: ['./exams-preview-general.component.scss']
})
export class ExamsPreviewGeneralComponent implements OnInit, OnDestroy {

  public model: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('CourseExams')
        .where('id').equal(params.id)
        .expand('course($expand=courseStructureType)')
        .getItem();
    });
    }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
