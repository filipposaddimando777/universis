import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-general-instructors',
  templateUrl: './exams-preview-general-instructors.component.html',
  styleUrls: ['./exams-preview-general-instructors.component.scss']
})
export class ExamsPreviewGeneralInstructorsComponent implements OnInit, OnDestroy  {

  courseExamID: any;
  public instructors: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseExamID = params.id;
      this.instructors = await this._context.model('CourseExams/' + params.id + '/instructors')
        .asQueryable()
        .expand('instructor')
        .prepare()
        .take(-1)
        .getItems();
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
