import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewTestTypesComponent } from './exams-preview-test-types.component';

describe('ExamsPreviewTestTypesComponent', () => {
  let component: ExamsPreviewTestTypesComponent;
  let fixture: ComponentFixture<ExamsPreviewTestTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewTestTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewTestTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
