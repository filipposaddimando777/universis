import { Component, OnInit } from '@angular/core';
import {TemplatePipe} from '@universis/common';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';
import * as USER_LIST_CONFIG from '../users-table/users-table.config.list.json';
import { TableConfiguration } from '../../../tables/components/advanced-table/advanced-table.interfaces';

@Component({
  selector: 'app-users-root',
  templateUrl: './users-root.component.html',
  styleUrls: ['./users-root.component.scss'],
  providers: [TemplatePipe]
})
export class UsersRootComponent implements OnInit {
  public user: any;
  public tabs: any[];
  public isCreate = false;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _template: TemplatePipe) {
  }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');

    if (this._activatedRoute.snapshot.params.id) {
      this.user = await this._context.model('users')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .getItem();
    }

    if (this.user) {
      // @ts-ignore
      this.config = cloneDeep(USER_LIST_CONFIG as TableConfiguration);

      if (this.config.columns) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action') {
              return x;
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = template(x.href)(this.user);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.user);
        });

      }

      return this._userActivityService.setItem({
        category: this._translateService.instant('Users.Title'),
        description: this._translateService.instant(this.user.name),
        url: window.location.hash.substring(1),
        dateCreated: new Date()
      });
    }
  }

}
