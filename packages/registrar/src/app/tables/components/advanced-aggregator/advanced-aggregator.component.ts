import { Component, Input, OnInit, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AdvancedAggregatorService } from './../../services/advanced-aggregator/advanced-aggregator.service';
import { AdvancedAggregatorConfig, AdvancedAggregatorFilters } from '../../advanced-aggregator';

/**
 * 
 * @class AdvancedAggregatorComponent
 * @classdesc An advanced list with the capability of aggregate data based on configurable filters
 * 
 */

@Component({
  selector: 'app-advanced-aggregator',
  templateUrl: './advanced-aggregator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdvancedAggregatorComponent implements OnInit, OnDestroy {

  private _config: AdvancedAggregatorConfig;
  private _aggregators;
  private _postProcessors;

  public body;
  public groups = [];

  @Input('config')
  public set config(value: AdvancedAggregatorConfig) {
    this._config = value;
  }

  @Input('aggregators')
  public set aggregators(value) {
    this._aggregators = value;
  }

  @Input('postProcessors')
  public set postProcessors(value) {
    this._postProcessors = value;
  }

  @Input('params') params;
  @Input('selectedGroups') selectedGroups$;
  @Input('filters') filters$;
  @Input('searchExpression') searchExpression: string;

  @Output() eventHandler = new EventEmitter();

  constructor(
    private _changeDetector: ChangeDetectorRef,
    private _advancedAggregatorService: AdvancedAggregatorService
  ) { }

  public get config() {
    return this._config;
  }

  public get aggregators() {
    return this._aggregators;
  }

  public get postProcessors() {
    return this._postProcessors;
  }

  async ngOnInit() {
    this.selectedGroups$.subscribe((data) => {
      this.groups = data;
      this._changeDetector.markForCheck();
    });

    this.filters$.subscribe(async (filters: AdvancedAggregatorFilters) => {
      try {
        await this.fetch(filters);
      } catch (err) {
        console.error(err);
      }
    });
  }

  async ngOnDestroy() {
    this.selectedGroups$.unsubscribe();
    this.filters$.unsubscribe();
  }

  /**
   * 
   * Retrieves the data 
   * 
   */
  async fetch(filters: AdvancedAggregatorFilters) {
    try {
      let data = await this._advancedAggregatorService.fetchData(this.config.model,
        this.params,
        this.config.query,
        filters.filters,
        this.searchExpression,
        filters.text
      );

      if (this.postProcessors && Object.keys(this.postProcessors).length > 0 && this.config.postProcessors && this.config.postProcessors.length > 0) {
        for (let i = 0; i < this.config.postProcessors.length; i++) {
          data = await this.postProcessors[this.config.postProcessors[i].name](data);
        }
      }

      this.body = this._advancedAggregatorService.formatData(this.config, data);
      this._changeDetector.markForCheck();
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * 
   * Triggers the event handler event
   * 
   * @param {*} event The event data
   * 
   */
  onActionEvent(event) {
    this.eventHandler.emit(event);
  }
}
