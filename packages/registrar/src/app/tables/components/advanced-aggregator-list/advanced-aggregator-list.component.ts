import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TemplatePipe} from '@universis/common';

enum Lengths {
  XL= 'xl',
  LG= 'lg',
  MD= 'md',
  SM= 'sm',
  XS= 'xs'
}


@Component({
  selector: 'app-advanced-aggregator-list',
  templateUrl: './advanced-aggregator-list.component.html'
})
export class AdvancedAggregatorListComponent implements OnInit {

  /**
   * @property {AdvancedAggregatorConfig} config The configuration object to use
   */
  @Input() config: any;

  /**
   * @property {Array<Array<any>>} body The list of data entries to show as the list body
   */
  @Input() body;

  @Output() clickEvent = new EventEmitter();

  constructor(private _template: TemplatePipe) { }

  ngOnInit() {
    this.config.columns.forEach(x => {
      x.length = Object.values(Lengths).includes(x.length) ? this._template.transform(`sis--list-group-item_${x.length}_column`, x ) :
        !x.length ? 'sis--list-group-item_sm_column': x.length ;
    })
  }

  /**
   *
   * Triggers the output event emitter
   *
   * @param type The type of the event as it's declared in the configuration
   * @param item The value of the current item
   * }
   */
  callCallback(type: string, data: any): void {
    this.clickEvent.emit({
      type,
      data
    });
  }
}
