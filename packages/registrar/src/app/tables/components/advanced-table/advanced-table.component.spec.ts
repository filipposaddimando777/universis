import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {AdvancedTableComponent, COLUMN_FORMATTERS} from './advanced-table.component';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import {TestingConfigurationService} from '../../../test';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';

describe('AdvancedTableComponent', () => {
  let component: AdvancedTableComponent;
  let fixture: ComponentFixture<AdvancedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedTableComponent ],
      imports: [
          CommonModule,
          RouterModule.forRoot([]),
          TranslateModule.forRoot(),
          MostModule.forRoot({
            base: '/',
            options: {
              useMediaTypeExtensions: false
            }
          })
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: COLUMN_FORMATTERS,
          useValue: []
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
