import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
// tslint:disable-next-line:max-line-length
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from 'packages/registrar/src/app/tables/components/advanced-table/advanced-table.component';
import { ActivatedTableService } from 'packages/registrar/src/app/tables/tables.activated-table.service';
import { Subscription } from 'rxjs';
import * as USERS_LIST_CONFIG from './departments-users.config.json';



@Component({
  selector: 'app-departments-preview-users',
  templateUrl: './departments-preview-users.component.html'
})

export class DepartmentsPreviewUsersComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> USERS_LIST_CONFIG;
  @ViewChild('users') users: AdvancedTableComponent;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private subscription: Subscription;
  public departmentId: any;
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService) { }

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      // get department id.
      this.departmentId = params.id;
      this._activatedTable.activeTable = this.users;
      // set users query
      this.users.query = this._context.model('Users')
          .asQueryable()
          .where('departments/id').equal(this.departmentId)
          .and('groups/name').equal('Registrar')
          .prepare();
      // set table config.
      this.users.config = AdvancedTableConfiguration.cast(USERS_LIST_CONFIG);
      // fetch users.
      this.users.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.users.fetch();
        }
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  removeUser() {
    if (this.users && this.users.selected && this.users.selected.length) {
      const items = this.users.selected.map( item => {
        const department = {id: this.departmentId};
        // set state for deletion.
        Object.defineProperty(department, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
        return {
          id: item.id,
          departments: [department]
        };
      });
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Departments.RemoveUser'),
        this._translateService.instant('Departments.RemoveUserMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          // save-remove items.
          this._context.model('Users').save(items).then( () => {
            // inform user.
            this._toastService.show(
              this._translateService.instant('Departments.RemoveUsersMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Departments.RemoveUsersMessage.one' : 'Departments.RemoveUsersMessage.many')
                , { value: items.length })
            );
            // fetch users.
            this.users.fetch();
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
