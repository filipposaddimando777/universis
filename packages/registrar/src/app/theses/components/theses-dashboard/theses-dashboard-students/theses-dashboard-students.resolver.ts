import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';

export class ThesesDashboardStudentsConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./theses-dashboard-students.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./theses-dashboard-students.config.list.json`);
      });
  }
}

/*export class ThesesDashboardStudentsSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./classes-instructors.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./theses-dashboard-students.search.list.json`);
      });
  }
}*/

export class ThesesDashboardDefaultStudentsConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./theses-dashboard-students.config.list.json`);
  }
}
