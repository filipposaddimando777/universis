import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import * as GRADUATIONS_REQUEST_LIST_CONFIG from './graduations-requests.config.json';
import * as GRADUATIONS_REQUEST_LIST_SEARCH from './graduations-requests.search.json';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ClientDataQueryable} from '@themost/client';
import {Observable, Subscription} from 'rxjs';
import {RequestActionComponent} from '../../../../requests/components/request-action/request-action.component';
import {AdvancedTableSearchComponent} from '../../../../tables/components/advanced-table/advanced-table-search.component';
import {ErrorService, LoadingService, ModalService, UserActivityService, UserService} from '@universis/common';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import {MessagesService} from '../../../../messages/services/messages.service';
import {SendMessageToStudentComponent} from '../../../../messages/components/send-message-to-student/send-message-to-student.component';

@Component({
  selector: 'app-graduations-student-requests',
  templateUrl: './graduations-student-requests.component.html'
})
export class GraduationsStudentRequestsComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private fragmentSubscription: Subscription;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  graduationEventId: any = this._activatedRoute.snapshot.params.id;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public allowActive = true;
  public recordsTotal: any;
  private selectedItems = [];
  constructor( private _context: AngularDataContext,
               private _activatedRoute: ActivatedRoute,
               private _modalService: ModalService,
               private _activatedTable: ActivatedTableService,
               private _loadingService: LoadingService,
               private _translateService: TranslateService,
               private _userActivityService: UserActivityService,
               private _userService: UserService,
               private _errorService: ErrorService,
               private _messagesService: MessagesService) { }

  async ngOnInit() {
    this.table.query = this._context.model('GraduationRequestActions')
      .where('graduationEvent').equal(this.graduationEventId)
      .expand('student($expand=studyProgram,department,person,semester,requeststatus,inscriptionYear,inscriptionPeriod,studentStatus)')
      .prepare();

    this.table.config = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_CONFIG);
    this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_SEARCH);

    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

    });

    const services = await this._context.model('diagnostics/services').getItems();
    if (services && services.length) {
      this.allowActive = !services.find(x => {
        return x.serviceType === 'ValidateGraduationRequestStudentStatus';
      });
    }
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus',
            'student/studentStatus/alternateName as studentStatus', 'student/id as studentId'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              studentStatus: item.studentStatus,
              student: item.studentId
            };
          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
     return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };

      // execute promises in series within an async method
      (async () => {
        const user = await this._userService.getUser();

        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              },
              agent: {
                id: user.id,
                name: user.name
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /***
   * Accepts the selected requests
   */
  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus' && ((!this.allowActive && item.studentStatus === 'declared') ||
          (this.allowActive && (item.studentStatus === 'active' || item.studentStatus === 'declared'))) ;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.AcceptAction.Title',
          description: 'Graduations.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.RejectAction.Title',
          description: 'Graduations.Edit.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async validateRequest() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.ValidateAction.Title',
          description: 'Graduations.Edit.ValidateAction.Description',
          refresh: this.refreshAction,
          execute: this.executeValidateRequest()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeValidateRequest() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };

      // execute promises in series within an async method
      (async () => {
        const user = await this._userService.getUser();

        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const requestValidations = await this._context.model(`GraduationRequestActions/${item.id}/validate`).getItems();
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async communicateWithAttendees() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      const selectedStudentIds = [];
      for (const item of this.selectedItems) {
        selectedStudentIds.push(item.student || item.studentId);
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(SendMessageToStudentComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          studentId: selectedStudentIds,
          showMessageForm: true,
          showLoading: false,
          modalTitle: 'Graduations.Edit.CommunicateWithAttendees.Title',
          description: 'Graduations.Edit.CommunicateWithAttendees.Description'
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
}
