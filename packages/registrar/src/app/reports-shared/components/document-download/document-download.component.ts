import {Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation, OnChanges} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import {ModalService, LoadingService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {NgxExtendedPdfViewerComponent} from 'ngx-extended-pdf-viewer';
import {DocumentSignComponent} from '../document-sign/document-sign.component';

declare var $: any;

@Component({
  selector: 'app-document-download',
  templateUrl: './document-download.component.html',
  styleUrls: ['../select-report/select-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocumentDownloadComponent implements OnInit, OnChanges, OnDestroy {

  @Input() blob: any;
  @Input() showSignButton = true;
  @Input() show = true;
  @Input() onClose: () => void | Promise<void>;
  @Input() documentCode: string;
  @Input() documentName: string;
  
  private subscription: Subscription;
  private changeSubscription: Subscription;
  private queryParamsSubscription: Subscription;
  public recordsTotal: number;
  public currentItem: any;
  public enableSignButton = true;

  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;

  constructor(private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _loadingService: LoadingService
            ) {}

  ngOnInit() {
    if (this.blob) {
      this.showViewer();
    }
  }

  ngOnChanges() {
    if (this.blob) {
      this.showViewer();
    }
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(documentCode: any) {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    this.currentItem = await this._context.model('DocumentNumberSeriesItems')
      .where('documentCode').equal(documentCode)
      .getItem();
    if (this.currentItem == null) {
      throw new ResponseError('Not Found', 404);
    }
    // enable of disable sign button
    this.enableSignButton = !this.currentItem.signed;
    const url = this.currentItem.url;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context.getService().resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.ok === false) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  showViewer() {
    this.setHeaderZIndex(true);
    $(this.pdfViewerContainer.nativeElement).show();
    this.pdfViewer.onResize();
    // add close button
    const toolbarViewerRight = $(this.pdfViewerContainer.nativeElement)
      .find('#toolbarViewerRight');
    if (toolbarViewerRight.find('pdf-close-tool').length === 0) {
      const closeText = this._translateService.instant('Reports.Viewer.Close');
      const closeTool = $(`
        <pdf-close-tool>
            <button style="width:auto" class="toolbarButton px-2" type="button">
                ${closeText}
            </button>
        </pdf-close-tool>
      `);
      // prepare to close viewer
      closeTool.find('button').on('click', () => {
        $(this.pdfViewerContainer.nativeElement).hide();
      });
      // insert before first which is the pdf hand tool
      closeTool.insertBefore(toolbarViewerRight.find('pdf-hand-tool'));
    }
  }

  closeViewer() {
    $(this.pdfViewerContainer.nativeElement).hide();
    this.setHeaderZIndex(false);
  }

  pdfClose() {
    this.setHeaderZIndex(false);
    try {
      $(this.pdfViewerContainer.nativeElement).hide();
      $('.bd-modal').removeClass('modal-pdf-viewer');
      if (this.onClose) {
        this.onClose();
      }
    } catch(err) {
      console.error(err);
    }
  }

  printAction() {
    $(this.pdfViewerContainer.nativeElement).find('pdf-print>button').trigger('click');
  }

  async downloadAction() {
    try {
      this._loadingService.showLoading();
      let documentName = 'document';
      if (this.documentName) {
        documentName = this.documentName;
      } else if (this.documentCode) {
        const item = await this._context.model('DocumentNumberSeriesItems')
        .where('documentCode').equal(this.documentCode)
        .getItem();
        documentName = item.name;
      }

      this.pdfViewer.filenameForDownload = documentName;
      this.pdfViewer.ngOnChanges({
        filenameForDownload: documentName as any
      });

      $(this.pdfViewerContainer.nativeElement).find('pdf-download>button').trigger('click');
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   * Opens an dialog to sign current document
   */
  async sign() {
    if (this.currentItem === null || typeof this.currentItem === 'undefined') {
      this.currentItem = await this._context.model('DocumentNumberSeriesItems')
      .where('documentCode').equal(this.documentCode)
      .getItem();
      if (this.currentItem === null || typeof this.currentItem === 'undefined') {
        throw new ResponseError('Not Found', 404);
      }
    }
    this._modalService.openModalComponent(DocumentSignComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
      initialState: {
        item: Object.assign({}, this.currentItem, {
          signReport: true
        })
      }
    });
  }

  /**
   * Sign document action
   */
  signAction() {
    return this.sign();
  }

  setHeaderZIndex(high: boolean) {
    const header = document.querySelector('header');
    if (header) {
      header.style.zIndex = high ? '1046' : null;
    }
  }
}
