import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: "app-verificate-code-form",
  templateUrl: "./verification-code-form.component.html"
})
export class VerificationCodeFormComponent implements OnInit, AfterViewInit {

  private static readonly LastVerificationCode = 'signer.lastVerificationCode';
  private static readonly LastVerificationCodeTime = 'signer.lastVerificationCodeTime';

  ngAfterViewInit(): void {
    setTimeout(() => {
      // get last verification code
      let lastTimeStr = sessionStorage.getItem(VerificationCodeFormComponent.LastVerificationCodeTime);
      let verificationCode;
      if (lastTimeStr) {
        verificationCode = sessionStorage.getItem(VerificationCodeFormComponent.LastVerificationCode);
        if (verificationCode) {
          const  lastTime = parseInt(lastTimeStr, 10);
          if (((new Date().getTime() - lastTime) / 1000) < 30)  {
            this.verificationCode = verificationCode;
            this.verificationInput.nativeElement.value = this.verificationCode;
          } else {
            // clear
            sessionStorage.removeItem(VerificationCodeFormComponent.LastVerificationCode);
            sessionStorage.removeItem(VerificationCodeFormComponent.LastVerificationCodeTime);
          }
        }
      }
      this.verificationInput.nativeElement.focus();
      // select text (if supported)
      setTimeout(() => {
        if (typeof this.verificationInput.nativeElement.select === 'function') {
          this.verificationInput.nativeElement.select();
        }
      }, 100)
    }, 500);
  }
  @ViewChild("verificationInput") verificationInput: ElementRef;
  @ViewChild("form", { read: NgForm }) form: any;
  @Output() verify: EventEmitter<any> = new EventEmitter(null);
  @Output() close: EventEmitter<any> = new EventEmitter(null);
  @Input() rememberVerificationCode = true;
  public verificationCode: string;

  ngOnInit(): void {
    //
  }

  doVerify() {
    if (this.rememberVerificationCode) {
      sessionStorage.setItem(VerificationCodeFormComponent.LastVerificationCode, this.verificationCode);
      sessionStorage.setItem(VerificationCodeFormComponent.LastVerificationCodeTime,new Date().getTime().toString());
    }
    return this.verify.emit(this.verificationCode);
  }

  onKeyEnter(event: KeyboardEvent) {
    if (event.code === 'Enter' && this.form && this.form.valid) {
      if (this.rememberVerificationCode) {
        sessionStorage.setItem(VerificationCodeFormComponent.LastVerificationCode, this.verificationCode);
        sessionStorage.setItem(VerificationCodeFormComponent.LastVerificationCodeTime,new Date().getTime().toString());
      }
       return this.verify.emit(this.verificationCode);
    }
}

}
