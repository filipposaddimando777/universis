import {Component, OnInit} from '@angular/core';
import {AppEventService, TemplatePipe} from '@universis/common';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {cloneDeep, template} from 'lodash';
import * as REGISTRATIONS_LIST_CONFIG from '../registrations-table/registrations-table.config.list.json';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ErrorService, LoadingService, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {RegistrationsService} from '../../services/registrations.service';

@Component({
  selector: 'app-registrations-root',
  templateUrl: './registrations-root.component.html',
})
export class RegistrationsRootComponent implements OnInit {
  public registration: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _registrationsService: RegistrationsService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _template: TemplatePipe,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    // @ts-ignore
    this.config = cloneDeep(REGISTRATIONS_LIST_CONFIG as TableConfiguration);

    this._activatedRoute.params.subscribe((params) => {
      this._context.model('StudentPeriodRegistrations')
        // .asQueryable()
        .where('id').equal(params.id)
        .expand('registrationPeriod,student($expand=person)')
        .getItem().then((registration) => {
        this.registration = registration;

        if (this.config.columns && this.registration) {
          this.actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);

          this.allowedActions = this.actions.filter(x => {
            if (x.role) {
              if (x.role === 'action') {
                if (x.access && x.access.length > 0) {
                  let access = x.access;
                  access = access.filter(y => {
                    if (y.studentStatuses) {
                      return y.studentStatuses.find(z => z.alternateName === this.registration.studentStatus.alternateName);
                    }
                  });
                  if (access && access.length > 0) {
                    return x;
                  }
                } else {
                  return x;
                }
              }
            }
          });

          this.edit = this.actions.find(x => {
            if (x.role === 'edit') {
              x.href = template(x.href)(this.registration);
              return x;
            }
          });

          this.actions = this.allowedActions;
          this.actions.forEach(action => {
            if (action.href.includes('${student}')) {
              action.href = action.href.replace('${student}', '${student.id}');
            }
            action.href = this._template.transform(action.href, this.registration);
          });

          if (this.registration) {
            return this._userActivityService.setItem({
              category: this._translateService.instant('Registrations.TitleOne'),
              // tslint:disable-next-line:max-line-length
              description: this._translateService.instant(this.registration.student.person.familyName + ' ' + this.registration.student.person.givenName),
              url: window.location.hash.substring(1),
              dateCreated: new Date()
            });
          }
        }
      }).catch((err) => {
        this._errorService.navigateToError(err);
      });
    });
    // if (this.config.columns && this.registration) {
    //   // get actions from config file
    //   this.actions = this.config.columns.filter(x => {
    //     return x.actions;
    //   })
    //     // map actions
    //     .map(x => x.actions)
    //     // get list items
    //     .reduce((a, b) => b, 0);

    //   // filter actions with student permissions
    //   this.allowedActions = this.actions.filter(x => {
    //     if (x.role) {
    //       if (x.role === 'action') {
    //         return x;
    //       }
    //     }
    //   });

    //   this.edit = this.actions.find(x => {
    //     if (x.role === 'edit') {
    //       x.href = template(x.href)(this.registration);
    //       return x;
    //     }
    //   });

    //   this.actions = this.allowedActions;
    //   this.actions.forEach(action => {
    //     action.href = template(action.href)(this.registration);
    //   });
    //   console.log(this.actions)

    // }

    // // @ts-ignore
    // this.config = cloneDeep(STUDENTS_LIST_CONFIG as TableConfiguration);


  }

  disableRegistration() {
    this._loadingService.showLoading();
    const status = {'alternateName': 'closed'};
    this._registrationsService.changeRegistrationStatus(this.registration, status).then((resAction) => {
      this.registration.status.alternateName = status.alternateName;
      this._appEvent.change.next({
        model: 'RegistrationsPreviewChange',
        target: this.registration
      });
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  enableRegistration() {
    this._loadingService.showLoading();
    const status = {'alternateName': 'open'};
    this._registrationsService.changeRegistrationStatus(this.registration, status).then((resAction) => {
      this.registration.status.alternateName = status.alternateName;
      this._appEvent.change.next({
        model: 'RegistrationsPreviewChange',
        target: this.registration
      });
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }
}
