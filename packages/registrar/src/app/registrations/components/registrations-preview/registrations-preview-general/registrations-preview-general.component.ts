import {Component, OnInit, Input, ViewChild, OnDestroy} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {Observable, Subscription} from 'rxjs';
import {EditCoursesComponent} from '../../../../study-programs/components/preview/edit-courses/edit-courses.component';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
  UserActivityService
} from '@universis/common';
import * as STUDENTPERIODREGISTRATIONS_LIST_CONFIG from './registrations-preview-general.config.list.json';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';
import {TranslateService} from '@ngx-translate/core';
import {AddItemsComponent} from '../../../../tables/components/add-items/add-items.component';
import {ClassesSharedModule} from '../../../../classes/classes.shared';
import {AdvancedTableEditorDirective} from "../../../../tables/directives/advanced-table-editor.directive";

@Component({
  selector: 'app-registrations-preview-general',
  templateUrl: './registrations-preview-general.component.html'
})
export class RegistrationsPreviewGeneralComponent implements OnInit, OnDestroy {

  public readonly config = STUDENTPERIODREGISTRATIONS_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private reloadSubscription: Subscription;

  private registrationId: any;
  public isCurrentRegistration = false;
  private availableClasses = [];
  public loading = true;
  public recordsTotal: any;
  private selectedItems = [];
  public registration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    this.isCurrentRegistration = false;

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.classes.fetch();
        }
      });
      this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
        if (data.tableConfiguration) {
          this.registrationId = parseInt(params.id, 10) || 0;
          this.classes.config = data.tableConfiguration;
          this.fetch ();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, {registration: this._activatedRoute.snapshot.params.id, activeDepartment: data.department.id});
          });
        }

        // loads registration data
        this._context.model(`StudentPeriodRegistrations`)
          .asQueryable()
          .where('id').equal(this.registrationId)
          .expand('student, classes')
          .getItem().then(async (item) => {
          if (item) {
            this.registration = item;
            if (this.registration) {

              // get request
              this.reloadSubscription = this._appEvent.change.subscribe(event => {
                if (event && event.target && event.model === 'RegistrationsPreviewChange') {
                  // reload request
                  if (event.target.id === this.registration.id) {
                    this.registration = event.target;
                  }
                }
              });

              const currentRegistration = await this._context.model(`students/${this.registration.student.id}/currentRegistration`)
                .asQueryable()
                .getItem();

              if (currentRegistration && this.registration.id === currentRegistration.id) {
                this.isCurrentRegistration = true;
                this.loading = false;
                // tslint:disable-next-line:max-line-length
                this.availableClasses = await this._context.model(`Students/${currentRegistration.student}/availableClasses`)
                  .asQueryable().take(-1).getItems();
              }
              // add user activity
              await this._userActivityService.setItem({
                category: this._translateService.instant('Registrations.TitleMany'),
                description: `${item.student.person.familyName} ${item.student.person.givenName}`,
                url: window.location.hash.substring(1), // get the path after the hash
                dateCreated: new Date
              });
            }
          }
        }).catch((err) => {
          this.loading = false;
        });
      });
    });
  }

  fetch () {
    const select = this.classes.config.columns.filter( column => {
      return column.virtual !== true;

    }).map(column => {
      if (column.property) {
        return column.name + ' as ' + column.property;
      }
      return column.name;
    });
    this._context.model('StudentCourseClasses')
      .select(...select)
      .where('registration').equal(this.registrationId)
      .take(-1)
      .getItems().then((items) => {
      this.tableEditor.set(items);

    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  executeEditAction() {
    return new Observable((observer) => {
      this._context.model('StudentCourseClasses').save(this.selectedItems).then(() => {
        this.classes.fetch(true);
        observer.next();
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  editMany() {
    if (this.classes.selected.length === 1) {
      // open edit modal window
      const urlTree = this._router.createUrlTree([{
        outlets: {
          modal: ['item', this.classes.selected[0].id, 'edit']
        }
      }], {
        relativeTo: this._activatedRoute
      });
      return this._router.navigateByUrl(urlTree).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    } else {
      this.selectedItems = this.classes.selected.map((x) => {
        return {
          id: x.id,
          units: x.units,
          coefficient: x.coefficient,
          semester: x.semester,
          ects: x.ects,
          courseType: x.courseType
        };
      });
      this._modalService.openModalComponent(EditCoursesComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.EditCourses',
          execute: this.executeEditAction()
        }
      });
    }
  }

  removeMany() {
    this.tableEditor.remove(...this.classes.selected);
  }

  async add() {
    try {
      const tableConfiguration =  AdvancedTableConfiguration.cast(ClassesSharedModule.AvailableClassList, true);
      tableConfiguration.model = `students/${this.registration.student.id}/availableClasses`;

      const courseTypes = await this._context.model(tableConfiguration.model)
        .select('courseType')
        .groupBy('courseType')
        .getItems();
      tableConfiguration.searches = courseTypes.map((item) => {
        return {
          'name': item.courseType.name,
          'value': item.courseType.id,
          'filter': {
            'courseTypeId': item.courseType.id
          },
          'emit': false
        };
      });

      this._modalService.openModalComponent(AddItemsComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          tableConfiguration: tableConfiguration,
          target: this.registration,
          modalTitle: 'Registrations.EditCourses',
          execute: (() => {
            return new Observable((observer) => {
              try {
                // get add courses component
                const component = <AddItemsComponent>this._modalService.modalRef.content;
                let courseClasses = [];
                const courseClassesFromTable = this.tableEditor.rows();

                component.items.forEach((item, index) => {
                  // @ts-ignore
                  let degree = courseClassesFromTable.find(e => e.courseClass === item.courseClass)
                  if(!degree){
                    courseClasses.push(item)
                  }
                })

                this.tableEditor.add(...courseClasses);
                observer.next();
              } catch (e) {
                observer.error(e);
              }
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async commit() {

    const dialogResult = await this._modalService.showDialog(
      this._translateService.instant('Registrations.CommitModal.Title'),
      this._translateService.instant('Registrations.CommitModal.Message'),
      DIALOG_BUTTONS.YesNo
    );
    if (dialogResult === 'no') {
      return;
    }

    const courseClasses = this.tableEditor.rows();
    this._loadingService.showLoading();
    const currentRegistration = await this._context.model(`StudentPeriodRegistrations`)
      .asQueryable()
      .where('id').equal(this.registrationId)
      .expand('student,classes')
      .getItem();

    currentRegistration.classes = courseClasses;

    this._context.model(`Students/${currentRegistration.student.id}/currentRegistration`)
      .save(currentRegistration).then((currentRegistrationResult) => {

      if (!currentRegistrationResult) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageRegistrationError'));

      }  else if (!currentRegistrationResult.validationResult) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageValidationError'));

      } else if (currentRegistrationResult.validationResult.success === false &&
        currentRegistrationResult.validationResult.code !== 'FAIL' &&
        currentRegistrationResult.validationResult.code !== 'EFAIL'
      ) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
        this._translateService.instant('Registrations.NoSuccessModal.Message'));

      } else if (currentRegistrationResult.validationResult.success === false ||
        currentRegistrationResult.validationResult.code === 'PSUCC' ||
        currentRegistrationResult.validationResult.code === 'FAIL' ||
        currentRegistrationResult.validationResult.code === 'EFAIL') {
        let message = '';

        message += currentRegistrationResult.validationResult.message;
        // tslint:disable-next-line:max-line-length
        message +=  currentRegistrationResult.validationResult.innerMessage ? ' : <br> ' + currentRegistrationResult.validationResult.innerMessage : '' ;
        message += '<br><br>';

        const classes = currentRegistrationResult.classes.filter(x => {
          if (x.validationResult) {
            if (!x.validationResult.success && x.name) {
              return x;
            }
          }
        });
        classes.forEach(x => {
          message += this._translateService.instant('Registrations.ParcialSuccessModal.Message',
            {name: x.name, message: x.validationResult.message}) + '<br><br>';
        });

        this._modalService.showErrorDialog(this._translateService.instant('Registrations.ParcialSuccessModal.Title'),
          message);

      }  else if (currentRegistrationResult.validationResult.success === false) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageValidationError'));

      } else if (currentRegistrationResult.validationResult.success) {
        this._toastService.show(
          this._translateService.instant('Registrations.SuccessToast.Title'),
          this._translateService.instant('Registrations.SuccessToast.Message')
        );

      } else {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.Message'));

      }

      this.fetch();
      this._loadingService.hideLoading();
    }).catch((err) => {
      this._loadingService.hideLoading();
      this._modalService.showErrorDialog('errorTitle', err.message);

    });
  }
}
