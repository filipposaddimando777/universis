import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class RegistrationsPreviewGeneralConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./registrations-preview-general.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./registrations-preview-general.config.list.json`);
      });
  }
}

export class RegistrationsPreviewGeneralSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./registrations-preview-general.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./registrations-preview-general.search.list.json`);
      });
  }
}

export class RegistrationsPreviewGeneralCoursesConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./registrations-preview-general.config.list.json`);
  }
}
