import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-registrations-preview',
  templateUrl: './registrations-preview.component.html'
})
export class RegistrationsPreviewComponent implements OnInit {

  @Input() StudentPeriodRegistration: any;
  public tabs: any[];

   constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private translate: TranslateService) {
  }

  async ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    this.StudentPeriodRegistration = await this._context.model('StudentPeriodRegistrations')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('registrationPeriod,student($expand=person)')
    .getItem();

  }

}
