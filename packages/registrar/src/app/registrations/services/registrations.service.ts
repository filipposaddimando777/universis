import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class RegistrationsService {

  constructor(private _context: AngularDataContext) { }

  changeRegistrationStatus(registration , status): any {
    return this._context.model(`StudentPeriodRegistrations`)
      .save({
        'id': registration.id,
        'status': {'alternateName': status.alternateName},
        'student' : registration.student,
        'registrationYear' : registration.registrationYear,
        'registrationPeriod' : registration.registrationPeriod
      });
  }

  saveCurrentRegistration(currentRegistration) {
    return this._context.model(`Students/${currentRegistration.student}/currentRegistration`)
      .save(currentRegistration).then((result) => {
        return result;
      }).catch((err) => {
        console.log(err);
      });
  }
}
